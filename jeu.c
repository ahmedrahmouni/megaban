#include <stdlib.h>
#include <stdio.h>
#include <SDL.h>
#include <SDL_image.h>

#include "constantes.h"
#include "jeu.h"


void jouer(SDL_Surface * ecran)
{

	SDL_Surface *megaman[4] = {NULL};
	// les quatres pour les directions de megaman(charactere du jeu)
	SDL_Surface *mur = NULL, *block = NULL, *blockOK = NULL, *objectif = NULL, *megamanActuel = NULL;
	//  block :square pushed, blockOK : block sur un objectif
	SDL_Rect position, positionJoueur;
	SDL_Event event;

	int continuer = 1, objectifsRestants = 0, i = 0, j = 0;
	//i,j pour parcourir le tableau carte
	int carte[NB_BLOCKS_LARGEUR][NB_BLOCKS_HAUTEUR] = {0};

	mur = IMG_Load("img/mur.png");
	block = IMG_Load("img/block.png");
	blockOK = IMG_Load("img/blockOK.png");
	objectif = IMG_Load("img/objectif.png");
	megaman[BAS] = IMG_Load("img/down.gif");
	megaman[GAUCHE] = IMG_Load("img/left.gif");
	megaman[HAUT] = IMG_Load("img/up.gif");
	megaman[DROITE] = IMG_Load("img/right.gif");

	megamanActuel = megaman[BAS];
	// Mario sera dirigé vers le bas au départ

// Chargement du niveau
	if (!chargerNiveau(carte))
	    exit(EXIT_FAILURE);
	    // le jeu s'arrete si le viveau n'a pas pu etre charger

// Recherche de la position de notre charactere au depart du jeu
	for(i = 0;i < NB_BLOCKS_LARGEUR ; i++)
	{
		for (j = 0; j < NB_BLOCKS_HAUTEUR ; j++)
		{
			if (carte[i][j] == MEGAMAN) // si megaman se trouve ici
			{
				positionJoueur.x = i;
				positionJoueur.y = j;
				carte[i][j] = VIDE;
			}
		}
	}

/* Activation de la repetition des touches pour qu'on
 puisse deplacer notre charactere */
	SDL_EnableKeyRepeat(100,100);

// Effacement de l'ecran
	SDL_FillRect(ecran, NULL, SDL_MapRGB(ecran->format, 255, 255, 255));

// Placement des obkets a l'ecran
	objectifsRestants = 0;
	for(i=0;i<NB_BLOCKS_LARGEUR;i++)
	{
		for(j=0;j<NB_BLOCKS_HAUTEUR;j++)
		{
			position.x = i * TAILLE_BLOC;
			position.y = j * TAILLE_BLOC;

			switch(carte[i][j])
			{
				case MUR:
					SDL_BlitSurface(mur, NULL, ecran, &position);
					break;
				case BLOCK:
					SDL_BlitSurface(block, NULL, ecran, &position);
					break;
				case BLOCK_OK:
					SDL_BlitSurface(blockOK, NULL, ecran, &position);
					break;
				case OBJECTIF:
					SDL_BlitSurface(objectif, NULL, ecran, &position);
					objectifsRestants = 1;
					break;
			}
		}
	}

//  s'il reste aucin objectif sur la carte cela veut dire qu'on a gagne
	if (!objectifsRestants)
		continuer = 0;

// placer le joueur a la bonne position

	position.x = positionJoueur.x * TAILLE_BLOC;
	position.y = positionJoueur.y * TAILLE_BLOC;
	SDL_BlitSurface(megamanActuel, NULL, ecran, &position);

	SDL_Flip(ecran);

// Desactivation de la  repetition des touches (on la remet a 0)
	SDL_EnableKeyRepeat(0,0);
//liberation des surfaces chargees
	SDL_FreeSurface(mur);
	SDL_FreeSurface(block);
	SDL_FreeSurface(blockOK);
	SDL_FreeSurface(objectif);
	for(i=0;i<4;i++)
	{
		SDL_FreeSurface(megaman[i]);
	}
}

void deplacerJoueur( int carte[][NB_BLOCKS_HAUTEUR],SDL_Rect *pos, int direction) //pos c'est la position de joueur
{
	switch(direction)
	{
		case HAUT:
			if(pos->y-1 < 0) // 1
				break;
			if (carte[pos->x][pos->y-1] ==MUR) // 2
				break;
			if ((carte[pos->x][pos->y-1]== BLOCK ||
				carte[pos->x][pos->y-1]== BLOCK_OK ) &&
				(pos-> y-2 <0 || carte[pos->x][pos->y-2]== MUR ||
				carte[pos->x][pos->y-2]==BLOCK ||carte[pos->x][pos->y-2]==BLOCK_OK))
				break; // 3
		/*  4   Si on passe les cas precedents ,on peut déplacer le joueur !
                On vérifie d'abord s'il y a une caisse à déplacer */
            deplacerCaisse(&carte[pos->x][pos->y - 1], &carte[pos->x][pos->y - 2]);
            pos-> y--; // 5 On peut faire monter le joueur
            break;

        case BAS:
            if (pos->y+1 >= NB_BLOCKS_HAUTEUR) // 1
                break;
            if (carte[pos->x][pos->y+1] == MUR) // 2
                break;

            if ((carte[pos->x][pos->y+1] == BLOCK ||
            	carte[pos->x][pos->y+1] == BLOCK_OK) &&
                (pos->y+2 >= NB_BLOCKS_HAUTEUR || carte[pos->x][pos->y+2] == MUR ||
                carte[pos->x][pos->y+2] == BLOCK || carte[pos->x][pos->y+2] == BLOCK_OK))
                break; // 3
            // 4
            deplacerCaisse(&carte[pos->x][pos->y + 1], &carte[pos->x][pos->y + 2]);

            pos->y++; // 5
            break;


        case GAUCHE:
            if (pos->x - 1 < 0) // 1
                break;
            if (carte[pos->x - 1][pos->y] == MUR) // 2
                break;

            if ((carte[pos->x - 1][pos->y] == BLOCK ||
            	carte[pos->x - 1][pos->y] == BLOCK_OK) &&
                (pos->x - 2 < 0 || carte[pos->x - 2][pos->y] == MUR ||
                carte[pos->x - 2][pos->y] == BLOCK ||
                carte[pos->x - 2][pos->y] == BLOCK_OK))
                break; //3
            // 4
            deplacerCaisse(&carte[pos->x - 1][pos->y], &carte[pos->x - 2][pos->y]);

            pos->x--; // 5
            break;


        case DROITE:
            if (pos->x + 1 >= NB_BLOCKS_LARGEUR) // 1
                break;
            if (carte[pos->x + 1][pos->y] == MUR) // 2
                break;

            if ((carte[pos->x + 1][pos->y] == BLOCK || carte[pos->x + 1][pos->y] == BLOCK_OK) &&
                (pos->x + 2 >= NB_BLOCKS_LARGEUR || carte[pos->x + 2][pos->y] == MUR ||
                carte[pos->x + 2][pos->y] == BLOCK || carte[pos->x + 2][pos->y] == BLOCK_OK))
                break; //3
            //4
            deplacerCaisse(&carte[pos->x + 1][pos->y], &carte[pos->x + 2][pos->y]);

            pos->x++;//5
            break;

/* Description fonction deplacerjoueur :

1)Si le joueur dépasse l'écran, on arrête

2)S'il y a un mur, on arrête

3)Si on veut pousser un block, il faut vérifier qu'il n'y a pas de mur derrière (ou un autre block, ou la limite du monde)

4)Si on passe les cas precedents ,on peut déplacer le joueur !
  On vérifie d'abord s'il y a une caisse à déplacer

5)On peut faire monter le joueur
*/

	}
}


void deplacerCaisse(int *premiereCase, int *secondeCase)
{
    if (*premiereCase == BLOCK || *premiereCase == BLOCK_OK)
    {
        if (*secondeCase == OBJECTIF)
            *secondeCase = BLOCK_OK;
        else
            *secondeCase = BLOCK;

        if (*premiereCase == BLOCK_OK)
            *premiereCase = OBJECTIF;
        else
            *premiereCase = VIDE;
    }
}



