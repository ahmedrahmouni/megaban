#include <stdio.h>
#include <stdlib.h>
#include <SDL.h>
#include <SDL_image.h>

#include "constantes.h"
#include "jeu.h"
#include "editeur.h"

/* la fonction main s'occupe des initialisations de la SDL */


int main(int argc, char *argv[])
{
	SDL_Surface *ecran = NULL, *menu = NULL;
	SDL_Rect positionMenu;
	SDL_Event event;

	int continuer = 1;

	SDL_Init(SDL_INIT_VIDEO);

	SDL_WM_SetIcon(IMG_Load("img/block.png"), NULL);
	ecran = SDL_SetVideoMode(LARGEUR_FENETRE, HAUTEUR_FENETRE, 64, SDL_HWSURFACE | SDL_DOUBLEBUF); // fonctions ecrites sur constantes.h
	SDL_WM_SetCaption("Mega-Man Sokoban", NULL);

	menu = IMG_Load("img/menu.jpg");  //affichage de menu d'acceuil
	positionMenu.x = 0;
	positionMenu.y = 0;

	while (continuer)
	{
		SDL_WaitEvent(&event);
		switch (event.type)
		{
		case SDL_QUIT:
			continuer = 0;
			break;
		case SDL_KEYDOWN:
			switch (event.key.keysym.sym)
			{
			case SDLK_ESCAPE: // arreter le jeu
				continuer = 0;
				break;
			case SDLK_KP1: // demande a jouer
				jouer(ecran);
				break;
			case SDLK_KP2: // demande l'editeur de niveaux
				editeur(ecran);
				break;
			}
			break;
		}

		//effacement de l'ecran
		SDL_FillRect(ecran, NULL, SDL_MapRGB(ecran->format, 0, 0, 0));
		SDL_BlitSurface(menu, NULL, ecran, &positionMenu);
		SDL_Flip(ecran);
	}

	SDL_FreeSurface(menu);
	SDL_Quit();  // arreter le SDL proprement

	return EXIT_SUCCESS;
}


