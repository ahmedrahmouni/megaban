/*
constantes.h
------------

Par Ahmed Rahmouni
Date de creation : 30/12/2019 .
Derniere Modif   : 30/12/2019 .

Rôle: définit des constantes pour tout le programme (taille de la fenêtre...)
*/

#ifndef DEF_CONSTANTES
#define DEF_CONSTANTES

	#define TAILLE_BLOC         64 //taille d'un bloc carre en pixels
	#define NB_BLOCKS_LARGEUR   12
	#define NB_BLOCKS_HAUTEUR   12
	#define LARGEUR_FENETRE     TAILLE_BLOC * NB_BLOCKS_LARGEUR 
	#define HAUTEUR_FENETRE     TAILLE_BLOC * NB_BLOCKS_HAUTEUR
    /* l'hauteur et la largeur de la fenetre du jeu depend de 
    la taille d'un bloc multiplier par le le nombre de la taille 
    horizentale ou verticale de la partie */
	
	enum {HAUT, BAS, GAUCHE, DROITE};
	enum {VIDE, MUR, BLOCK, OBJECTIF, MEGAMAN, BLOCK_OK};

#endif